
// requires c++14 for function scoped class return type
auto test()
{
	class
	{
	public:
		int value{12345};
	} instance;

	return instance;
}

void test_ambiguous()
{
	// --- For later work on Amb function --- //
	
	std::cout << " ---------------------------------- \n";
	auto does_this_work = test();
	std::cout << does_this_work.value << std::endl;
	
	// ------------------------------- //
}
