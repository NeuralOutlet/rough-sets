
#include <iostream>
#include "set.hpp"

BucketNode::BucketNode(ElementBase *_item)
{
	next = nullptr;
	item = _item;
}

BucketNode::~BucketNode()
{
	if (item)
	{
		delete item;
	}
}

Bucket::Bucket()
	: p_front{nullptr}
	, p_rear{nullptr}
{}

Bucket::~Bucket()
{
	if (p_front == p_rear)
	{
		if (p_front)
		{
			delete p_front;
		}

		p_front = p_rear = nullptr;
	}

	if (p_front)
	{
		delete p_front;
	}

	if (p_rear)
	{
		delete p_rear;
	}
}

BucketNode * Bucket::front()
{
	return p_front;
}

// ------------------------

Set::Set()
	: p_hashTable{new Bucket[10]}
	, m_capacity{10}
	, m_size{0}
{}

Set::~Set()
{
	delete [] p_hashTable;
}

unsigned int Set::size()
{
	return m_size;
}

unsigned int Set::hash(ElementBase *item)
{
	return item->hash() % m_capacity;
}

