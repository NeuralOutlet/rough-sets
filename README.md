# Sets Project

Implementations of a variadic set class, a rough set class, and McCarthy's 'Ambiguous operator'.

Variadic set:

* hash table implementation (using std::hash)
* insert, search, remove.
* insert makes a copy and takes ownership, is then immutable
* has iterator and other container QoL


