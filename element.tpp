
template<typename T>
Element<T>::Element(T value)
	: m_value{value}
{}

template<typename T>
T Element<T>::value()
{
	return m_value;
}

template<typename T>
const std::type_info & Element<T>::type()
{
	return m_typeID;
}

template<typename T>
bool Element<T>::typeCheck(ElementBase *elem)
{
	return elem->type() == m_typeID;
}

template<typename T>
int Element<T>::hash()
{
	return std::hash<T>{}(m_value);
}

template<typename Type>
bool element_cast(ElementBase *elem, Type &value)
{
	if (Element<Type>::typeCheck(elem))
	{
		Element<Type> *temp = dynamic_cast<Element<Type> *>(elem);

		if (temp) // non-nullptr
		{
			value = temp->value();
			return true;
		}
	}

	return false;
}


