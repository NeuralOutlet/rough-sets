#ifndef SET_H
#define SET_H

#include <iostream>
#include "element.hpp"

struct BucketNode
{
	BucketNode *next;
	ElementBase *item;
	
	BucketNode(ElementBase *_item);
	~BucketNode();
};

class Bucket
{
public:
	Bucket();
	~Bucket();

	template<typename Type>
	bool insert(ElementBase *item);
	
	template<typename Type>
	bool search(ElementBase *item);

	template<typename Type>
	bool remove(ElementBase *item);

	BucketNode * front();

private:
	template<typename Type>
	bool search(BucketNode* node, ElementBase *item);
	
	template<typename Type>
	bool remove(BucketNode* node, BucketNode *prevNode, ElementBase *item);

private:
	BucketNode *p_front;
	BucketNode *p_rear;
};


class Set
{
public:
	Set();
	~Set();

	template<typename Type>
	bool insert(Element<Type> elem);

	template<typename Type>
	bool search(Element<Type> elem);

	template<typename Type>
	bool remove(Element<Type> elem);

	/* // Container methods
	const ElementBaseBase * cbegin() const noexcept;
	const ElementBaseBase * cend()   const noexcept;
	const ElementBaseBase * begin() const noexcept;
	const ElementBaseBase * end()   const noexcept;
	ElementBaseBase * begin() noexcept;
	ElementBaseBase * end()   noexcept;
	// ++iter/iter++
	*/

	unsigned int size();

private:
	unsigned int hash(ElementBase *item);

private:
	Bucket *p_hashTable;
	unsigned int m_capacity;
	unsigned int m_size;
};

// include template definitions
#include "set.tpp"

#endif // SET_H
