
template<typename Type>
bool Bucket::insert(ElementBase *item)
{
	if (p_rear == nullptr) // first entry in the bucket
	{
		BucketNode* temp = new BucketNode(item);
		p_front = p_rear = temp;
	}
	else
	{
		if (search<Type>(p_front, item)) // check for duplicate
		{
			return false;
		}
		else
		{
			BucketNode* temp = new BucketNode(item);
			p_rear->next = temp;
			p_rear = temp;
		}
	}

	return true;
}

template<typename Type>
bool Bucket::search(BucketNode* node, ElementBase *item)
{
	bool keepGoing = true;
	if (node == p_rear)
		keepGoing = false;

	bool match = true;
	Type itemValue, nodeItemValue;
	match &= element_cast<Type>(item, itemValue);
	match &= element_cast<Type>(node->item, nodeItemValue);

	if (match && itemValue == nodeItemValue)
	{
		return true;
	}
	else
	{
		if (keepGoing)
			return search<Type>(node->next, item);
	}

	return false;
}

template<typename Type>
bool Bucket::remove(ElementBase *item)
{
	if (p_front == nullptr)
		return false;

	return remove<Type>(p_front, p_front, item);
}

template<typename Type>
bool Bucket::remove(BucketNode* node, BucketNode* prevNode, ElementBase *item)
{
	bool keepGoing = true;
	if (node == p_rear)
		keepGoing = false;

	bool match = true;
	Type itemValue, nodeItemValue;
	match &= element_cast<Type>(item, itemValue);
	match &= element_cast<Type>(node->item, nodeItemValue);

	if (match && itemValue == nodeItemValue)
	{
		prevNode->next = node->next;
		delete node;

		if (p_rear == p_front)
		{
			p_rear = p_front = nullptr;
		}

		return true;
	}
	else
	{
		if (keepGoing)
			return remove<Type>(node->next, node, item);
	}

	return false;
}

template<typename Type>
bool Bucket::search(ElementBase *item)
{
	if (p_front == nullptr)
		return false;

	return search<Type>(p_front, item);
}


template<typename Type>
bool Set::insert(Element<Type> elem)
{
	bool result = false; // expect failure
	ElementBase * item = new Element<Type>(elem);

	int index = hash(item);
	if (p_hashTable[index].insert<Type>(item))
	{
		++m_size;
		result = true;
	}

	delete item;

	return result;
}

template<typename Type>
bool Set::search(Element<Type> elem)
{
	ElementBase *item = new Element<Type>(elem);

	int index = hash(item);
	bool result = p_hashTable[index].search<Type>(item);

	delete item;

	return result;
}

template<typename Type>
bool Set::remove(Element<Type> elem)
{
	bool result = false; // expect failure
	ElementBase *item = new Element<Type>(elem);

	int index = hash(item);
	if (p_hashTable[index].remove<Type>(item))
	{
		--m_size;
		result = true;
	}

	delete item;

	return result;
}
