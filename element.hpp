#include <iostream>
#include <functional>

/**
	ElementBase
	Used for a polymorphic pointer, contains only virtual functions
*/
class ElementBase
{
public:
	// Type specific virtual methods
	virtual const std::type_info & type() = 0;
	virtual int hash() = 0;
};

/**
	Element<T>
	Derrived from ElementBase for polymorphic pointer.
	Stores data of type T.
*/
template<typename T>
class Element : public ElementBase
{
public:
	Element(T value);

	const std::type_info & type() override;
	int hash() override;

	static bool typeCheck(ElementBase *elem);
	T value();

private:
	static const std::type_info & m_typeID;
	T m_value;
};

template<typename T>
const std::type_info & Element<T>::m_typeID = typeid(T);


/**
	element_cast
	A global function used to safely typecheck
	an element and extract the value within.
*/
template<typename Type>
bool element_cast(ElementBase *elem, Type &value);

#include "element.tpp"
