
#include "set.hpp"
#include "test_sets.hpp"

int main()
{
	run_test("insert: int", &test_insert_int);
	run_test("insert: float", &test_insert_float);
	run_test("insert: string", &test_insert_string);
	run_test("insert: variadic", &test_insert_int);
	run_test("insert: duplicate", &test_insert_duplicates);
	run_test("search: variadic", &test_search);
	run_test("remove: duplicate", &test_remove);
	run_test("remove: odds", test_remove_odds);

	return 0;
}
