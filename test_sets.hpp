
#include "set.hpp"

void run_test(std::string testname, bool (*testfunc)(Set&));

bool test_insert_int(Set&);
bool test_insert_float(Set&);
bool test_insert_string(Set&);
bool test_insert_variadic(Set&);
bool test_insert_duplicates(Set&);
bool test_search(Set&);
bool test_remove(Set&);
bool test_remove_odds(Set&);
