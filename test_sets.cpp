
#include "test_sets.hpp"

#include <iostream>
#include <string>

void run_test(std::string testname, bool (*testfunc)(Set&))
{
	std::cout << " --- " << testname << " --- " << std::endl;

	Set myset;
	if (testfunc(myset))
	{
		std::cout << "\n     PASSED\n" << std::endl;
	}
	else
	{
		std::cout << "\n     FAILED\n" << std::endl;
	}
}

bool test_remove_odds(Set &myset)
{
	bool result = true;
	const int max_items = 10;

	for (int i = 0; i < max_items; ++i)
		myset.insert<int>(i);

	for (int i = 0; i < max_items; ++i)
		if (i % 2 == 1)
			myset.remove<int>(i);

	for (int i = 0; i < max_items; ++i) {
		if (i % 2 == 1) {
			result &= !myset.search<int>(i);
		} else {
			result &= myset.search<int>(i);
		}
	}

	return result;
}

bool test_insert_variadic(Set &myset)
{
	bool result = true;

	result &= (myset.size() == 0);

	myset.insert<int>(100);
	result &= (myset.size() == 1);

	myset.insert<float>(1.618033f);
	result &= (myset.size() == 2);

	myset.insert<std::string>(std::string("variadic"));
	result &= (myset.size() == 3);

	return result;
}

bool test_insert_int(Set &myset)
{
	bool result = true;

	result &= (myset.size() == 0);

	myset.insert<int>(100);
	result &= (myset.size() == 1);

	myset.insert<int>(-54321);
	result &= (myset.size() == 2);

	myset.insert<int>(42);
	result &= (myset.size() == 3);

	return result;
}

bool test_insert_float(Set &myset)
{
	bool result = true;

	result &= (myset.size() == 0);

	myset.insert<float>(100);
	result &= (myset.size() == 1);

	myset.insert<float>(1.618033f);
	result &= (myset.size() == 2);

	myset.insert<float>(-123.00005);
	result &= (myset.size() == 3);

	return result;
}

bool test_insert_string(Set &myset)
{
	bool result = true;

	result &= (myset.size() == 0);

	myset.insert<std::string>(std::string("aye"));
	result &= (myset.size() == 1);

	myset.insert<std::string>(std::string("bee"));
	result &= (myset.size() == 2);

	myset.insert<std::string>(std::string("cee"));
	result &= (myset.size() == 3);

	return result;
}

bool test_search(Set &myset)
{
	bool result = true;

	myset.insert<int>(100);
	myset.insert<float>(1.618033f);
	myset.insert<std::string>(std::string("variadic"));

	result &= (myset.search<int>(100) ==  true);
	result &= (myset.search<int>(99) == false);
	result &= (myset.search<std::string>(std::string("variadic")) == true);
	result &= (myset.search<double>(1.618033f) == false);
	result &= (myset.search<float>(1.618033f) == true);

	return result;
}

bool test_remove(Set &myset)
{
	bool result = true;

	myset.insert<int>(1);
	myset.insert<int>(2);
	myset.insert<int>(3);
	result &= (myset.size() == 3);

	myset.remove<int>(2);
	result &= (myset.size() == 2);

	myset.remove<int>(7);
	result &= (myset.size() == 2);

	result &= (myset.search<int>(1) ==  true);
	result &= (myset.search<int>(2) ==  false);
	result &= (myset.search<int>(3) ==  true);

	return result;
}

bool test_insert_duplicates(Set &myset)
{
	bool result = true;

	result &= (myset.size() == 0);

	myset.insert<int>(1);
	result &= (myset.size() == 1);

	myset.insert<double>(1);
	result &= (myset.size() == 2);

	myset.insert<int>(1);
	result &= (myset.size() == 2);

	myset.insert<int>(1);
	result &= (myset.size() == 2);

	myset.insert<int>(1.0f);
	result &= (myset.size() == 2);

	myset.insert<unsigned int>(1);
	result &= (myset.size() == 3);

	return result;
}
